import requests
import re
import feedparser
import time
import datetime
from bs4 import BeautifulSoup

def rssParse(url):
    feedData = feedparser.parse(url)

    return feedData['entries']

def stateOfFloridaParse(url):
    jobsList=[]

    # Parse State of Florida Job Data
    result = requests.get(url)
    c = result.content
    soup = BeautifulSoup(c,"html.parser")

    # Get Total Pages
    pagesNumberString = soup.find_all("span", "srHelp hidden")[0].get_text()
    pageRegEx = "[0-9]+"
    pageSearchResults = re.findall(pageRegEx,pagesNumberString)
    totalPages = int(pageSearchResults[1])

    for pageNum in range(0,totalPages):
        if pageNum != 0:
            newURL = url + "&startrow=" + str((pageNum)*25)
            result = requests.get(newURL)
            c = result.content
            soup = BeautifulSoup(c, "html.parser")

        # Extract Data Row Information
        jobRows = soup.find_all("tr", "data-row")

        for row in jobRows:

            jobUrl = "https://jobs.myflorida.com" + str(row.find_all('a', 'jobTitle-link')[0]['href'])
            jobTitle = str(row.find_all('a', 'jobTitle-link')[0].get_text())
            location = str(row.find_all('span', 'jobLocation')[0].get_text())
            jobDate = str(row.find_all('span', 'jobDate')[0].get_text())
            jobDate = jobDate.replace("\n","")
            jobDate = jobDate.replace("\t","")
            jobDate = datetime.datetime.fromtimestamp(time.mktime(time.strptime(jobDate, "%b %d, %Y")))
            jobDept = str(row.find_all('span', 'jobDepartment')[0].get_text())
            jobFacility = str(row.find_all('span', 'jobFacility')[0].get_text())

            jobData = {'title':jobTitle,'url':jobUrl, 'agency':'State of Florida', 'subAgency':jobFacility, 'jobCategory':jobDept, 'location':location, 'jobDate':jobDate}

            jobsList.append(jobData)

    return jobsList

def federal(url, usaJobsEmailAddress, usaJobsAPIKey):
    jobsList = []

    header = {'User-Agent':usaJobsEmailAddress,'Authorization-Key':usaJobsAPIKey}

    acceptableLocations = ["Daytona Beach, Florida", "Ormond Beach, Florida", "Port Orange, Florida", "South Daytona, Florida", "Ponce Inlet, Florida", "DeLand, Florida", "Daytona Beach Shores, Florida", "Holly Hill, Florida", "Deltona, Florida", "Lake Helen, Florida",
                           "Edgewater, Florida", "Oak Hill, Florida", "DeBary, Florida", "Pierson, Florida", "New Smyrna Beach, Florida"]

    result = requests.get(url,headers=header).json()

    for job in result['SearchResult']['SearchResultItems']:

        location = None

        for location in job['MatchedObjectDescriptor']['PositionLocation']:
            if location['CityName'] in acceptableLocations:
                location = location['CityName']

        jobUrl = job['MatchedObjectDescriptor']['PositionURI']
        jobTitle = job['MatchedObjectDescriptor']['PositionTitle']
        jobDate = job['MatchedObjectDescriptor']['PublicationStartDate']
        jobDate = datetime.datetime.strptime(jobDate[:10],'%Y-%m-%d')
        jobDept = job['MatchedObjectDescriptor']['JobCategory'][0]['Name']
        jobFacility = job['MatchedObjectDescriptor']['OrganizationName']
        jobAgency = job['MatchedObjectDescriptor']['DepartmentName']

        jobData = {'title': jobTitle, 'url': jobUrl, 'agency': jobAgency, 'subAgency': jobFacility,
                   'jobCategory': jobDept, 'location': location, 'jobDate': jobDate}

        jobsList.append(jobData)

    return jobsList

def agencyParse(data,name,type):
    jobsList = []
    if type == "GovernmentJobs":
        for entry in data:

            jobUrl = entry['id']
            jobTitle = entry['title']
            location = entry['joblisting_location']
            jobDate = datetime.datetime.fromtimestamp(time.mktime(entry['published_parsed']))
            jobDept = entry['categorycode']
            jobFacility = entry['joblisting_department']

            jobData = {'title': jobTitle, 'url': jobUrl, 'agency': name, 'subAgency': jobFacility,'jobCategory': jobDept, 'location': location, 'jobDate': jobDate}
            jobsList.append(jobData)
        return jobsList

    elif type == "CivicPlus":
        for entry in data:
            jobUrl = entry['link']
            jobTitle = entry['title']
            location = ""
            jobDate = datetime.datetime.fromtimestamp(time.mktime(entry['published_parsed']))
            jobDept = ""
            jobFacility = ""

            jobData = {'title': jobTitle, 'url': jobUrl, 'agency': name, 'subAgency': jobFacility, 'jobCategory': jobDept,
                       'location': location, 'jobDate': jobDate}
            jobsList.append(jobData)
        return jobsList

    elif type == "eGov":
        for entry in data:

            jobUrl = entry['id']
            jobTitle = entry['title']
            location = ""
            jobDate = datetime.datetime.fromtimestamp(time.mktime(entry['updated_parsed']))
            jobDept = ""
            jobFacility = ""

            jobData = {'title': jobTitle, 'url': jobUrl, 'agency': name, 'subAgency': jobFacility,'jobCategory': jobDept, 'location': location, 'jobDate': jobDate}
            jobsList.append(jobData)
        return jobsList

    elif type == "VirtualTowns":
        for entry in data:

            jobUrl = entry['link']
            jobTitle = entry['title']
            location = ""
            jobDate = datetime.datetime.fromtimestamp(time.mktime(entry['published_parsed']))
            jobDept = ""
            jobFacility = ""

            jobData = {'title': jobTitle, 'url': jobUrl, 'agency': name, 'subAgency': jobFacility,'jobCategory': jobDept, 'location': location, 'jobDate': jobDate}
            jobsList.append(jobData)
        return jobsList

