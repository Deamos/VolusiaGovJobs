import uuid
import json
import hashlib
import os
from operator import itemgetter

import atexit

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.interval import IntervalTrigger

# Import Flask
from flask import Flask, render_template

from functions import parsings

usaJobsEmailAddress = ""
usaJobsAPIKey = ""

sysPath = os.path.dirname(os.path.abspath(__file__))

app = Flask(__name__)

app.secret_key = str(uuid.uuid4())

jobsList = []

# Functions

def format_date(dtObj):
    stringDT = str(dtObj)[:-9]
    return stringDT

def reformat_category(catStr):
    result = catStr

    strTable = {
        "911Telecommunications":"911/Telecommunications",
        "ParksandBeaches":"Parks and Beaches",
        "LawEnforcement":"Law Enforcement",
        "ConstructionMaintenance":"Construction/Maintenance",
        "PlanningandDevelopment":"Planning and Development",
        "CodeEnforcement":"Code Enforcement",
        "PublicWorks":"Public Works",
        "CommunityDevelopment":"Community Development",
        "OfficeAdminSupport":"Office Admin Support",
        "AccountingandFinance":"Accounting and Finance",
        "PublicSafety":"Public Safety",
        "ITandComputers":"IT and Computers",
        "CustomerService":"Customer Service",
        "Criminology":"Law Enforcement",
        "FireEMS":"Fire/EMS",
        "ParksandRecreation":"Parks and Recreation",
        "HospitalityTourism":"Hospitality/Tourism"
    }

    if catStr in strTable:
        result = strTable[catStr]

    return result

def get_md5(strValue):
    return hashlib.md5(strValue).hexdigest()

def retrieveFeeds():

    global sysPath

    feedList = None

    data = []

    jsonFile = os.path.join(sysPath, 'feeds.json')

    with open(jsonFile) as json_data:
        feedList = json.load(json_data)

    for feed in feedList['feeds']:
        if feed['type'] is not "None":
            if feed['type'] == "State Of Florida":
                jobs = parsings.stateOfFloridaParse(feed['url'])
                for job in jobs:
                    data.append(job)

            elif feed['type'] == "Federal":
                jobs = parsings.federal(feed['url'], usaJobsEmailAddress, usaJobsAPIKey)
                for job in jobs:
                    data.append(job)

            else:
                jobs = parsings.rssParse(feed['url'])

                if feed['type'] == "GovernmentJobs" or feed['type'] == "CivicPlus" or feed['type'] == "eGov" or feed['type'] == "VirtualTowns":

                    jobs = parsings.agencyParse(jobs, feed['name'], feed['type'])
                    for job in jobs:
                        data.append(job)

                else:
                    for job in jobs:
                        data.append(job)

    return data


def retrieveFeedsBG():
    global sysPath
    global jobsList

    feedList = None

    data = []

    jsonFile = os.path.join(sysPath, 'feeds.json')

    with open(jsonFile) as json_data:
        feedList = json.load(json_data)

    for feed in feedList['feeds']:
        if feed['type'] is not "None":
            if feed['type'] == "State Of Florida":
                jobs = parsings.stateOfFloridaParse(feed['url'])
                for job in jobs:
                    data.append(job)

            elif feed['type'] == "Federal":
                jobs = parsings.federal(feed['url'], usaJobsEmailAddress, usaJobsAPIKey)
                for job in jobs:
                    data.append(job)

            else:
                jobs = parsings.rssParse(feed['url'])

                if feed['type'] == "GovernmentJobs" or feed['type'] == "CivicPlus" or feed['type'] == "eGov" or feed[
                    'type'] == "VirtualTowns":

                    jobs = parsings.agencyParse(jobs, feed['name'], feed['type'])
                    for job in jobs:
                        data.append(job)

                else:
                    for job in jobs:
                        data.append(job)

    jobsList = data

# Configurations
app.jinja_env.filters['format_date'] = format_date
app.jinja_env.filters['reformat_category'] = reformat_category




#Initialization

#Initialize Scheduler
scheduler = BackgroundScheduler()
scheduler.start()
scheduler.add_job(
    func=retrieveFeedsBG,
    trigger=IntervalTrigger(minutes=30),
    id='background_job_get_json_data',
    name='Download JSON data from sources and import them into the DB',
    replace_existing=True)
# Shut down the scheduler when exiting the app
atexit.register(lambda: scheduler.shutdown())

# Run Scheduler First Time
for job in scheduler.get_jobs():
    job.func()

@app.route('/')
def main_page():

    global jobsList

    if jobsList is not None:
        newlist = sorted(jobsList, key=itemgetter('jobDate'),reverse=True)
        jobsList = newlist

    elif jobsList is None:
        jobsList = retrieveFeeds()
        newlist = sorted(jobsList, key=itemgetter('jobDate'),reverse=True)
        jobsList = newlist

    return render_template('index.html',jobsList=jobsList)

if __name__ == '__main__':
    app.jinja_env.auto_reload = True
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    app.run(debug=False, host='0.0.0.0',port=5000)